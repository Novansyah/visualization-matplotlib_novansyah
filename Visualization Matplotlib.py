#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


import numpy as np
x = np.linspace(0,5,11)
y = x ** 2
x


# In[3]:


y


# In[4]:


plt.plot(x,y,'r')
plt.xlabel('X Axis title')
plt.ylabel('Y Axis tittle')
plt.title('String tittle')
plt.show()


# In[5]:


plt.subplot(1,2,1)
plt.plot(x,y,'r--')
plt.subplot(1,2,2)
plt.plot(y, x,'g*-')


# In[6]:


fig = plt.figure()
axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])
axes.plot(x, y, 'b')
axes.set_xlabel('Set X label')
axes.set_ylabel('Set Y label')
axes.set_title('Set title')


# In[7]:


fig = plt.figure()
axes1 = fig.add_axes([0.1, 0.1, 0.8,0.8])
axes2 = fig.add_axes([0.2, 0.5, 0.4, 0.3])

axes1.plot(x, y, 'b')
axes1.set_xlabel('X_label_axes2')
axes1.set_ylabel('Y_label_axes2')
axes1.set_title('Axes 2 title')

axes2.plot(x, y, 'r')
axes2.set_xlabel('X_label_axes2')
axes2.set_ylabel('Y_label_axes2')
axes2.set_title('Axes 2 title')


# In[8]:


fig, axes = plt.subplots()

axes.plot(x, y, 'r')
axes.set_xlabel('X')
axes.set_ylabel('Y')
axes.set_title('title')


# In[9]:


fig, axes = plt.subplots(nrows = 1, ncols=2)


# In[10]:


axes


# In[11]:


for ax in axes:
    ax.plot(x, y, 'r')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_title('title')
fig


# In[12]:


fig, axes = plt.subplots(nrows = 1, ncols=2)
for ax in axes:
    ax.plot(x, y, 'g')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_title('title')
fig
plt.tight_layout()


# In[13]:


fig = plt.figure(figsize = (8,4), dpi = 100)


# In[14]:


fig, axes = plt.subplots(figsize = (12,3))

axes.plot(x, y, 'r')
axes.set_xlabel('X')
axes.set_ylabel('Y')
axes.set_title('title')


# In[15]:


fig.savefig("filename.png")


# In[16]:


fig.savefig("filename.png", dpi = 200)


# In[17]:


ax.set_title("title");


# In[18]:


ax.set_xlabel("x")
ax.set_xlabel("y")


# In[19]:


fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.plot(x, x**2, label = "x**2")
ax.plot(x, x**3, label = "x**3")
ax.legend()


# In[20]:


ax.legend(loc=1)
ax.legend(loc=2)
ax.legend(loc=3)
ax.legend(loc=4)

ax.legend(loc=0)
fig


# In[21]:


fig, ax = plt.subplots()
ax.plot(x, x**2, 'b.-')
ax.plot(x, x**3, 'g--')


# In[22]:


fig, ax = plt.subplots()
ax.plot(x, x+1, color = "blue", alpha=0.5)
ax.plot(x, x+2, color = "#8B008B")
ax.plot(x, x+3, color = "#FF8C00")


# In[23]:


fig, ax = plt.subplots(figsize=(12,6))

ax.plot(x, x+1, color = "red", linewidth = 0.25)
ax.plot(x, x+2, color = "red", linewidth = 0.50)
ax.plot(x, x+3, color = "red", linewidth = 1.00)
ax.plot(x, x+4, color = "red", linewidth = 2.00)

ax.plot(x, x+5, color = "green", lw = 3, linestyle='-')
ax.plot(x, x+6, color = "green", lw = 3,ls='-.')
ax.plot(x, x+7, color = "green", lw = 3, ls = ':')

line, = ax.plot(x, x+8, color="black", lw = 1.50)
line.set_dashes([5,10,15,10])

ax.plot(x, x+9, color = "blue", lw = 3, ls = '-', marker = '+')
ax.plot(x, x+10, color = "blue", lw = 3, ls = '--', marker = 'o')
ax.plot(x, x+11, color = "blue", lw = 3, ls = '-', marker = 's')
ax.plot(x, x+12, color = "blue", lw = 3, ls = '--', marker = '1')

ax.plot(x, x+13, color = "purple", lw = 1, ls = '-', marker = 'o', markersize=2)
ax.plot(x, x+14, color = "purple", lw = 1, ls = '-', marker = 'o', markersize=4)
ax.plot(x, x+15, color = "purple", lw = 1, ls = '-', marker = 'o', markersize=8, markerfacecolor = 'red')
ax.plot(x, x+16, color = "purple", lw = 1, ls = '-', marker = 's', markersize=8,
       markerfacecolor = "yellow", markeredgewidth = 3, markeredgecolor = "green");


# In[24]:


fig, axes = plt.subplots(1,3,figsize=(12,4))

axes[0].plot(x,x**2, x, x**3)
axes[0].set_title("default axes range")

axes[1].plot(x, x**2, x, x**3)
axes[1].axis('tight')
axes[1].set_title("tight axes")

axes[2].plot(x, x**2, x, x**3)
axes[2].set_ylim([0, 60])
axes[2].set_xlim([2,5])
axes[2].set_title("custom axes range");


# In[25]:


plt.scatter(x,y)


# In[26]:


from random import sample
data = sample(range(1,1000), 100)
plt.hist(data)


# In[27]:


data = [np.random.normal(0, std, 100) for std in range(1, 4)]
plt.boxplot(data, vert = True, patch_artist = True);


# In[28]:


import numpy as np
import pandas as pd
get_ipython().run_line_magic('matplotlib', 'inline')


# In[29]:


df1 = pd.read_csv('df1', index_col = 0)
df2 = pd.read_csv('df2')


# In[30]:


df1['A'].hist()


# In[31]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[32]:


df1['A'].hist()


# In[33]:


plt.style.use('bmh')
df1['A'].hist()


# In[34]:


df1['A'].hist()


# In[35]:


plt.style.use('ggplot')


# In[36]:


df2.head()


# In[37]:


df2.plot.bar()


# In[38]:


df2.plot.bar(stacked= True)


# In[39]:


df2.plot.area(alpha=0.4)


# In[40]:


df1['A'].plot.hist(bins=50)


# In[41]:


df1['Year']=df1.index
df1.plot.line(x='Year', y='B', figsize=(12,3),lw =1)


# In[42]:


df1.plot.scatter(x = 'A', y='B')


# In[43]:


df1.plot.scatter(x='A',y='B', c='C', cmap = 'coolwarm')


# In[44]:


df1.plot.scatter(x='A', y='B',s=df1['C']*200)


# In[45]:


df2.plot.box()


# In[46]:


df = pd.DataFrame(np.random.randn(1000,2), columns =['a','b'])
df.plot.hexbin(x='a', y='b', gridsize=25, cmap='Oranges')


# In[47]:


pip install scipy


# In[48]:


df2['a'].plot.kde()


# In[49]:


df2.plot.density()


# In[51]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

mcdon = pd.read_csv('mcdonalds.csv', index_col='Date', parse_dates = True)
mcdon.head()


# In[52]:


mcdon.plot()


# In[53]:


mcdon['Adj. Close'].plot()


# In[54]:


mcdon['Adj. Volume'].plot()


# In[55]:


mcdon['Adj. Close'].plot(figsize=(12,8))


# In[56]:


mcdon['Adj. Close'].plot(figsize=(12,8))
plt.ylabel('Close Price')
plt.xlabel('Overwrite Date Index')
plt.title('Mcdonalds')


# In[57]:


mcdon['Adj. Close'].plot(figsize = (12,8), title = 'Pandas Title')


# In[59]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2009-01-01'])


# In[60]:


mcdon['Adj. Close'].plot(xlim = ['2007-01-01', '2009-01-01'],ylim=[0,50])


# In[61]:


mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'], ylim=[0,40], ls='--', c='r')


# In[62]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates
mcdon['Adj. Close'].plot(xlim=['2007-01-01','2007-05-01'], ylim=[0,40])


# In[63]:


idx = mcdon.loc['2007-01-01':'2007-05-01'].index
stock = mcdon.loc['2007-01-01':'2007-05-01']['Adj. Close']
idx


# In[64]:


stock


# In[65]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')
plt.tight_layout()
plt.show()


# In[66]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[67]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock, '-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[68]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('%b\n%Y'))
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[69]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')
ax.yaxis.grid(True)
ax.xaxis.grid(True)
ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n\n\n%Y--%B'))
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[70]:


fig, ax = plt.subplots()
ax.plot_date(idx, stock,'-')

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n%Y--%B'))

ax.xaxis.set_major_locator(dates.MonthLocator())
ax.xaxis.set_major_formatter(dates.DateFormatter('%d'))
ax.yaxis.grid(True)
ax.xaxis.grid(True)
fig.autofmt_xdate()
plt.tight_layout()
plt.show()


# In[71]:


fig, ax = plt.subplots(figsize=(10,8))
ax.plot_date(idx, stock,'-')

ax.xaxis.set_major_locator(dates.WeekdayLocator(byweekday=1))
ax.xaxis.set_major_formatter(dates.DateFormatter('%B-%d-%a'))

ax.yaxis.grid(True)
ax.xaxis.grid(True)

fig.autofmt_xdate()
plt.tight_layout()
plt.show()

