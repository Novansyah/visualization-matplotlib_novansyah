#!/usr/bin/env python
# coding: utf-8

# In[7]:


import numpy as np
x = np.arange(0,100)
y = x*2
z = x*2


# In[8]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.show()


# In[9]:


plt.plot(y,z,'r')
plt.xlabel('X Axis title')
plt.ylabel('Y Axis tittle')
plt.title('String tittle')
plt.show()


# In[10]:


print('hi')


# In[26]:


fig = plt.figure()
axes1 = fig.add_axes([0, 0, 1,1])
axes2 = fig.add_axes([0.2, 0.5, .2, .2])

axes.plot(x, y, 'b')
axes.set_xlabel('X_label_axes2')
axes.set_ylabel('Y_label_axes2')
axes.set_title('Axes 2 title')

axes.plot(x, y, 'r')
axes.set_xlabel('X_label_axes2')
axes.set_ylabel('Y_label_axes2')
axes.set_title('Axes 2 title')


# In[35]:


import numpy as np
a = np.arange(0,100)
b = a*2
c = a**2


# In[39]:


fig = plt.figure()
axes1 = fig.add_axes([0, 0, 1,1])
axes1 = fig.add_axes([0.2, 0.5, .4, .4])

axes1.plot(a, c, 'b')
axes1.set_xlabel('X_label_axes2')
axes1.set_ylabel('Y_label_axes2')
axes1.set_title('Axes 2 title')

axes1.plot(a, c, 'r')
axes1.set_xlabel('X_label_axes2')
axes1.set_ylabel('Y_label_axes2')
axes1.set_title('Axes 2 title')


# In[41]:


fig = plt.figure()
axes1 = fig.add_axes([0, 0, 1,1])
axes2 = fig.add_axes([0.2, 0.5, .4, .4])

axes1.plot(a, c, 'b')
axes1.set_xlabel('X_label_axes2')
axes1.set_ylabel('Y_label_axes2')
axes1.set_title('Axes 2 title')

axes2.plot(a, b, 'r')
axes2.set_xlabel('X_label_axes2')
axes2.set_ylabel('Y_label_axes2')
axes2.set_title('zoom')


# In[43]:


fig, axes = plt.subplots(nrows=1, ncols=2)


# In[44]:


axes


# In[48]:


for ax in axes:
    ax.plot(a,c,'b--')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('title')
fig


# In[49]:


import pandas as pd
import matplotlib.pyplot as plt
df3 = pd.read_csv('df3')
get_ipython().run_line_magic('matplotlib', 'inline')


# In[50]:


df3.info()


# In[51]:


df3.head()


# In[53]:


df3.plot.scatter(x='a', y='b')


# In[55]:


df3['a'].plot.hist()


# In[56]:


import matplotlib.pyplot as plt
plt.style.use('ggplot')


# In[57]:


df3['a'].hist()


# In[58]:


df3.plot.box()


# In[61]:


df3['a'].plot.kde()


# In[65]:


df3.plot.area(alpha=0.5)


# In[ ]:




